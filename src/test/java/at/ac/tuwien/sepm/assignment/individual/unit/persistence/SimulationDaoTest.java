package at.ac.tuwien.sepm.assignment.individual.unit.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.ISimulationDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class SimulationDaoTest {

    @Autowired
    ISimulationDao simulationDao;
    @Autowired
    DBConnectionManager dbConnectionManager;

    /**
     * It is important to close the database connection after each test in order to clean the in-memory database
     */
    @After
    public void afterEachTest() throws PersistenceException {
        dbConnectionManager.closeConnection();
    }

    @Test(expected = NotFoundException.class)
    public void givenNothing_whenFindSimulationByIdWhichNotExists_thenNotFoundException()
        throws PersistenceException, NotFoundException {
        simulationDao.findOneById(1);
    }

    @Test
    public void createOneSimulationAndRetrieve() throws PersistenceException, NotFoundException {
        //given
        Simulation entity = getSimulation();
        //when
        Simulation Simulation = simulationDao.createSimulation(entity);
        Simulation result = simulationDao.findOneById(Simulation.getId());
        //then
        assertNotNull(result);
    }

    private Simulation getSimulation() {
        Simulation entity = new Simulation();
        entity.setName("hans");
        entity.setParticipants(new ArrayList<>());
        return entity;
    }
}

