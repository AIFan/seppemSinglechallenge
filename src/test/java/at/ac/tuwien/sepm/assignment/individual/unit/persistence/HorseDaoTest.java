package at.ac.tuwien.sepm.assignment.individual.unit.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IHorseDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class HorseDaoTest {

    @Autowired
    IHorseDao horseDao;
    @Autowired
    DBConnectionManager dbConnectionManager;

    /**
     * It is important to close the database connection after each test in order to clean the in-memory database
     */
    @After
    public void afterEachTest() throws PersistenceException {
        dbConnectionManager.closeConnection();
    }

    @Test(expected = NotFoundException.class)
    public void givenNothing_whenFindHorseByIdWhichNotExists_thenNotFoundException()
        throws PersistenceException, NotFoundException {
        horseDao.findOneById(100);
    }

    @Test
    public void createOneHorseAndRetrieve() throws PersistenceException, NotFoundException {
        //given
        Horse entity = getHorse();
        //when
        Horse horse = horseDao.createHorse(entity);
        Horse result = horseDao.findOneById(horse.getId());
        //then
        assertNotNull(result);
    }

    private Horse getHorse() {
        Horse entity = new Horse();
        entity.setName("hans");
        entity.setBreed("horse");
        entity.setMinSpeed(45.2);
        entity.setMaxSpeed(57.3);
        return entity;
    }

    @Test(expected = NotFoundException.class)
    public void updateHorseNotExisting() throws NotFoundException, PersistenceException {
        //given
        Horse entity = new Horse();
        entity.setId(1);
        //when
        horseDao.updateHorse(entity);
    }

    @Test
    public void updateHorseAndRetrieve() throws PersistenceException, NotFoundException {
        //given
        Horse entity = getHorse();
        Horse horse = horseDao.createHorse(entity);
        String newName = "Hannes";
        String newBreed = "pony";
        Double newMinSpeed = 48.0;
        Double newMaxSpeed = 59.0;
        entity.setId(horse.getId());
        entity.setName(newName);
        entity.setBreed(newBreed);
        entity.setMinSpeed(newMinSpeed);
        entity.setMaxSpeed(newMaxSpeed);
        //when
        Horse result = horseDao.updateHorse(entity);
        //then
        assertNotNull(result);
        assertEquals(newName, result.getName());
        assertEquals(newBreed, result.getBreed());
        assertEquals(newMinSpeed, result.getMinSpeed());
        assertEquals(newMaxSpeed, result.getMaxSpeed());
    }

    @Test(expected = NotFoundException.class)
    public void deleteHorse() throws PersistenceException, NotFoundException {
        //given
        Horse entity = getHorse();
        Horse horse = horseDao.createHorse(entity);
        //when
        horseDao.deleteOneById(horse.getId());
        //then
        horseDao.findOneById(horse.getId());
    }
}

