package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;

abstract class AbstractHorseTest {
    Horse createMinimalHorse() {
        Horse minimalHorse = new Horse();
        minimalHorse.setName("Hans");
        minimalHorse.setMinSpeed(40.0);
        minimalHorse.setMaxSpeed(60.0);
        return minimalHorse;
    }
}
