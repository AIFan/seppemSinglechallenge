package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.api.dto.SimulationResultDto;
import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.service.IHorseService;
import at.ac.tuwien.sepm.assignment.individual.service.IJockeyService;
import at.ac.tuwien.sepm.assignment.individual.service.ISimulationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class SimulationServiceTest {
    @Autowired
    private ISimulationService simulationService;
    @Autowired
    private IHorseService horseService;
    @Autowired
    private IJockeyService jockeyService;

    @Test(expected = NotFoundException.class)
    public void createSimulationNotFound() throws NotFoundException, ServiceException {
        //given
        Simulation sim = new Simulation(1, "name", null);
        sim.setParticipants(Collections.singletonList(new Participant(1, 1.0, 1, 1)));
        //when / then
        simulationService.createSimulation(sim);
    }

    //19 - 1 (valid)
    @Test
    public void createSimulation() throws NotFoundException, ServiceException {
        //given
        horseService.createHorse(new Horse(1, "hans", null, 42.0, 47.0, null, null));
        jockeyService.createJockey(new Jockey(1, "jock", 900.0, null, null));
        Simulation sim = new Simulation(1, "name", null);
        sim.setParticipants(Collections.singletonList(new Participant(1, 1.0, 1, 1)));
        //when
        simulationService.createSimulation(sim);
        //then
        SimulationResultDto simulationResult = simulationService.getSimulationResultForId(1);
        assertNotNull(simulationResult);
    }

    //19 - 2 (invalid luck factor)
    @Test(expected = IllegalArgumentException.class)
    public void createSimulationLuckFactor0() throws NotFoundException, ServiceException {
        //given
        horseService.createHorse(new Horse(1, "hans", null, 42.0, 47.0, null, null));
        jockeyService.createJockey(new Jockey(1, "jock", 900.0, null, null));
        Simulation sim = new Simulation(1, "name", null);
        sim.setParticipants(Collections.singletonList(new Participant(1, 0.0, 1, 1)));
        //when
        simulationService.createSimulation(sim);
    }

    //19 - 3 (invalid skill)
    @Test(expected = IllegalArgumentException.class)
    public void createSimulationSkillNaN() throws NotFoundException, ServiceException {
        //given
        horseService.createHorse(new Horse(1, "hans", null, 42.0, 47.0, null, null));
        jockeyService.createJockey(new Jockey(1, "jock", Double.NaN, null, null));
        Simulation sim = new Simulation(1, "name", null);
        sim.setParticipants(Collections.singletonList(new Participant(1, 1.0, 1, 1)));
        //when
        simulationService.createSimulation(sim);
    }

    //19 - 4 (invalid min Speed)
    @Test(expected = IllegalArgumentException.class)
    public void createSimulationMinSpeedBelow40() throws NotFoundException, ServiceException {
        //given
        horseService.createHorse(new Horse(1, "hans", null, 39.0, 47.0, null, null));
        jockeyService.createJockey(new Jockey(1, "jock", 10.0, null, null));
        Simulation sim = new Simulation(1, "name", null);
        sim.setParticipants(Collections.singletonList(new Participant(1, 1.0, 1, 1)));
        //when
        simulationService.createSimulation(sim);
    }
}
