package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IJockeyDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.service.IJockeyValidationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.util.mapper.JockeyMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class JockeyServiceUnitTest {
    @Mock
    private IJockeyDao JockeyDao;

    @Mock
    private IJockeyValidationService JockeyValidationService;

    @Mock
    private JockeyMapper JockeyMapper;

    @InjectMocks
    private JockeyService service;

    @Test
    public void createJockey() throws ServiceException, PersistenceException {
        //given
        Jockey dto = createMinimalJockey();
        //when
        service.createJockey(dto);
        //then
        verify(JockeyValidationService).validateCreationOfAJockey(any());
        verify(JockeyDao).createJockey(any());
    }

    private Jockey createMinimalJockey() {
        return new Jockey(1,"name",12.0,null,null);
    }

    @Test
    public void updateJockey() throws NotFoundException, ServiceException, PersistenceException {
        //given
        Jockey h = createMinimalJockey();
        h.setId(1);
        //when
        service.updateJockey(h);
        //then
        verify(JockeyValidationService).validateIdOfAJockey(any());
        verify(JockeyDao).updateJockey(any());
    }

    @Test
    public void deleteJockey() throws ServiceException, PersistenceException, NotFoundException {
        service.deleteOneById(1);
        verify(JockeyDao).deleteOneById(1);
    }

    @Test
    public void getAllJockeys() throws ServiceException, PersistenceException {
        //given
        //when
        service.getAllJockeys(new Jockey());
        //then
        verify(JockeyDao).getAllJockeys(any());
    }
}
