package at.ac.tuwien.sepm.assignment.individual.unit.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IJockeyDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class JockeyDaoTest {

    @Autowired
    IJockeyDao jockeyDao;
    @Autowired
    DBConnectionManager dbConnectionManager;

    /**
     * It is important to close the database connection after each test in order to clean the in-memory database
     */
    @After
    public void afterEachTest() throws PersistenceException {
        dbConnectionManager.closeConnection();
    }

    @Test(expected = NotFoundException.class)
    public void givenNothing_whenFindJockeyByIdWhichNotExists_thenNotFoundException()
        throws PersistenceException, NotFoundException {
        jockeyDao.findOneById(1);
    }

    @Test
    public void createOneJockeyAndRetrieve() throws PersistenceException, NotFoundException {
        //given
        Jockey entity = getJockey();
        //when
        Jockey Jockey = jockeyDao.createJockey(entity);
        Jockey result = jockeyDao.findOneById(Jockey.getId());
        //then
        assertNotNull(result);
    }

    private Jockey getJockey() {
        Jockey entity = new Jockey();
        entity.setName("hans");
        entity.setSkill(12.2);
        return entity;
    }

    @Test(expected = NotFoundException.class)
    public void updateJockeyNotExisting() throws NotFoundException, PersistenceException {
        //given
        Jockey entity = new Jockey();
        entity.setId(1);
        //when
        jockeyDao.updateJockey(entity);
    }

    @Test
    public void updateJockeyAndRetrieve() throws PersistenceException, NotFoundException {
        //given
        Jockey entity = getJockey();
        Jockey Jockey = jockeyDao.createJockey(entity);
        String newName = "Hannes";
        Double newSkill = 59.0;
        entity.setId(Jockey.getId());
        entity.setName(newName);
        entity.setSkill(newSkill);
        //when
        Jockey result = jockeyDao.updateJockey(entity);
        //then
        assertNotNull(result);
        assertEquals(newName, result.getName());
        assertEquals(newSkill, result.getSkill());
    }

    @Test(expected = NotFoundException.class)
    public void deleteJockey() throws PersistenceException, NotFoundException {
        //given
        Jockey entity = getJockey();
        Jockey Jockey = jockeyDao.createJockey(entity);
        //when
        jockeyDao.deleteOneById(Jockey.getId());
        //then
        jockeyDao.findOneById(Jockey.getId());
    }
}

