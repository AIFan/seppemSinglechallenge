package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import org.junit.Test;

public class HorseValidationServiceUnitTest extends AbstractHorseTest {
    private HorseValidationService service = new HorseValidationService();

    @Test(expected = IllegalArgumentException.class)
    public void createHorseInvalidMinSpeed() {
        //given
        Horse h = createMinimalHorse();
        h.setMinSpeed(39.3);
        //when/then
        service.validateMinAndMaxSpeedOfAHorse(h);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createHorseInvalidMaxSpeed() {
        //given
        Horse h = createMinimalHorse();
        h.setMaxSpeed(61.0);
        //when/then
        service.validateMinAndMaxSpeedOfAHorse(h);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createHorseMissingArguments() {
        //given
        Horse h = createMinimalHorse();
        h.setName(null);
        //when/then
        service.validateCreationOfAHorse(h);
    }

    @Test(expected = IllegalArgumentException.class)
    public void missingId() {
        //given
        Horse h = createMinimalHorse();
        //when/then
        service.validateIdOfAHorse(h);
    }

}
