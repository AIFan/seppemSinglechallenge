package at.ac.tuwien.sepm.assignment.individual.unit.persistence;

import at.ac.tuwien.sepm.assignment.individual.persistence.util.CriteriaBuilder;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CriteriaBuilderTest {
    @Test
    public void appendEqualsParamTest() {
        //given
        String sql = "";
        //when
        sql = CriteriaBuilder.appendEqualsParam("name","hans", sql);
        sql = CriteriaBuilder.appendEqualsParam("breed","Bree", sql);
        sql = CriteriaBuilder.appendEqualsParam("breed",null, sql);
        //then
        assertEquals("name = ? AND breed = ?",sql);
    }

    @Test
    public void appendGreaterOrEqualsParamTest() {
        //given
        String sql = "";
        //when
        sql = CriteriaBuilder.appendGreaterOrEqualsParam("skill",0.5d, sql);
        sql = CriteriaBuilder.appendGreaterOrEqualsParam("minSpeed",42.d, sql);
        sql = CriteriaBuilder.appendGreaterOrEqualsParam("maxSpeed",null, sql);
        //then
        assertEquals("skill >= ? AND minSpeed >= ?",sql);
    }

    @Test
    public void appendLesserOrEqualsTest() {
        //given
        String sql = "";
        //when
        sql = CriteriaBuilder.appendLesserOrEqualsParam("skill",0.5d, sql);
        sql = CriteriaBuilder.appendLesserOrEqualsParam("minSpeed",42.d, sql);
        sql = CriteriaBuilder.appendLesserOrEqualsParam("maxSpeed",null, sql);
        //then
        assertEquals("skill <= ? AND minSpeed <= ?",sql);
    }

    @Test
    public void appendLikeParamTest() {
        //given
        String sql = "";
        //when
        sql = CriteriaBuilder.appendLikeParam("name","hans", sql);
        sql = CriteriaBuilder.appendLikeParam("breed","Bree", sql);
        sql = CriteriaBuilder.appendLikeParam("breed",null, sql);
        //then
        assertEquals("name like ? AND breed like ?",sql);
    }
}
