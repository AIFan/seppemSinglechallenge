package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IHorseDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.service.IHorseValidationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.util.mapper.HorseMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HorseServiceUnitTest extends AbstractHorseTest {
    @Mock
    private IHorseDao horseDao;

    @Mock
    private IHorseValidationService horseValidationService;

    @Mock
    private HorseMapper horseMapper;

    @InjectMocks
    private HorseService service;

    @Test
    public void createHorse() throws ServiceException, PersistenceException {
        //given
        Horse dto = createMinimalHorse();
        //when
        service.createHorse(dto);
        //then
        verify(horseValidationService).validateCreationOfAHorse(any());
        verify(horseDao).createHorse(any());
    }

    @Test
    public void updateHorse() throws NotFoundException, ServiceException, PersistenceException {
        //given
        Horse h = createMinimalHorse();
        h.setId(1);
        //when
        service.updateHorse(h);
        //then
        verify(horseValidationService).validateIdOfAHorse(any());
        verify(horseDao).updateHorse(any());
    }

    @Test
    public void deleteHorse() throws ServiceException, PersistenceException, NotFoundException {
        service.deleteOneById(1);
        verify(horseDao).deleteOneById(1);
    }

    @Test
    public void getAllHorses() throws ServiceException, PersistenceException {
        //given
        //when
        service.getAllHorses(new Horse());
        //then
        verify(horseDao).getAllHorses(any());
    }
}
