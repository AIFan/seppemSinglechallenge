package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.entity.HorseSnapshot;
import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;


/**
 * Represents a data access object for the horse snapshot table.
 */
public interface IHorseSnapshotDao {
    /**
     * @param id of the horseSnapshot to find.
     * @return the horseSnapshot with the specified id.
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the horseSnapshot could not be found in the database.
     */
    HorseSnapshot findOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity the input parameters to create the new horseSnapshot
     * @param participant
     * @param jockey
     * @return the id of the created horseSnapshot
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    HorseSnapshot createHorseSnapshot(Horse entity, Participant participant, Jockey jockey) throws PersistenceException;

    /**
     * @param id of the horseSnapshot to delete
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    void deleteOneById(Integer id) throws PersistenceException, NotFoundException;
}
