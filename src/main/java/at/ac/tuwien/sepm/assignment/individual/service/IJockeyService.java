package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;

import java.util.List;


/**
 * Represents a service for operations on the jockey
 */
public interface IJockeyService {

    /**
     * @param id of the jockey to find.
     * @return the jockey with the specified id.
     * @throws ServiceException  will be thrown if something goes wrong during data processing.
     * @throws NotFoundException will be thrown if the jockey could not be found in the system.
     */
    Jockey findOneById(Integer id) throws ServiceException, NotFoundException;

    /**
     * @param entity the entity containing the data of the jockey to create
     * @return the id of the created jockey
     * @throws ServiceException         will be thrown if something goes wrong during data processing.
     * @throws IllegalArgumentException will be thrown if the input parameters don't match the requirements.
     */
    Jockey createJockey(Jockey entity) throws ServiceException, IllegalArgumentException;

    /**
     * @param entity the entity containing the data of the jockey to update
     * @return the updated jockey
     * @throws ServiceException         will be thrown if something goes wrong during data processing.
     * @throws IllegalArgumentException will be thrown if the input parameters don't match the requirements.
     */
    Jockey updateJockey(Jockey entity) throws ServiceException, IllegalArgumentException, NotFoundException;

    /**
     * @param id of the jockey to delete
     */
    void deleteOneById(Integer id) throws ServiceException, NotFoundException;

    /**
     * @param entity a entity containing filter values
     * @return a list containing all existing jockeys
     * @throws ServiceException will be thrown if something goes wrong during data processing.
     */
    List<Jockey> getAllJockeys(Jockey entity) throws ServiceException;
}
