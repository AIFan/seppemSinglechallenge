package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.entity.JockeySnapshot;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;


/**
 * Represents a data access object for the jockey snapshot table.
 */
public interface IJockeySnapshotDao {
    /**
     * @param id of the jockeySnapshot to find.
     * @return the jockeySnapshot with the specified id.
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the jockeySnapshot could not be found in the database.
     */
    JockeySnapshot findOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity the input parameters to create the new jockeySnapshot
     * @return the id of the created jockeySnapshot
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    JockeySnapshot createJockeySnapshot(Jockey entity) throws PersistenceException;

    /**
     * @param id of the jockeySnapshot to delete
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    void deleteOneById(Integer id) throws PersistenceException, NotFoundException;
}
