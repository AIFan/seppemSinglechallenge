package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;


/**
 * Represents a service for the validation on the jockey
 */
public interface IJockeyValidationService {
    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if the id in the entity is null
     */
    void validateIdOfAJockey(Jockey entity) throws IllegalArgumentException;

    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if the entity is missing required parameters
     *                                  or validateMinAndMaxSpeedOfAJockey is not valid
     */
    void validateCreationOfAJockey(Jockey entity) throws IllegalArgumentException;

    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if no value is set
     */
    void validateIfJockeyIsEmpty(Jockey entity) throws IllegalArgumentException;
}
