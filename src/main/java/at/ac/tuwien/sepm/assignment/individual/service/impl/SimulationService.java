package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.api.dto.ParticipantResultDto;
import at.ac.tuwien.sepm.assignment.individual.api.dto.SimulationResultDto;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.entity.SimulationParticipant;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.*;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.impl.SimulationDao;
import at.ac.tuwien.sepm.assignment.individual.service.ISimulationService;
import at.ac.tuwien.sepm.assignment.individual.service.ISimulationValidationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SimulationService implements ISimulationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationService.class);
    private final ISimulationDao simulationDao;
    private final ISimulationParticipantDao simulationParticipantDao;
    private final IHorseSnapshotDao horseSnapshotDao;
    private final IJockeySnapshotDao jockeySnapshotDao;
    private final IParticipantDao participantDao;
    private final ISimulationValidationService simulationValidationService;

    @Autowired
    public SimulationService(SimulationDao simulationDao, ISimulationParticipantDao simulationParticipantDao, IHorseSnapshotDao horseSnapshotDao, IJockeySnapshotDao jockeySnapshotDao, IParticipantDao participantDao, ISimulationValidationService simulationValidationService) {
        this.simulationDao = simulationDao;
        this.simulationParticipantDao = simulationParticipantDao;
        this.horseSnapshotDao = horseSnapshotDao;
        this.jockeySnapshotDao = jockeySnapshotDao;
        this.participantDao = participantDao;
        this.simulationValidationService = simulationValidationService;
    }

    @Override
    public Simulation createSimulation(Simulation entity) throws ServiceException, NotFoundException, IllegalArgumentException {
        LOGGER.info("Create simulation");
        try {
            simulationValidationService.validateSimulation(entity);
            return simulationDao.createSimulation(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Simulation> getAllSimulations(Simulation entity) throws ServiceException {
        LOGGER.info("Get all simulations.");
        try {
            return simulationDao.getAllSimulations(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public SimulationResultDto getSimulationResultForId(int simulationId) throws ServiceException, NotFoundException {
        try {
            var sim = simulationDao.findOneById(simulationId);
            var result = new SimulationResultDto(sim.getId(), sim.getName(), sim.getCreated(), new ArrayList<>());
            List<SimulationParticipant> simPartMapping = simulationParticipantDao.getAllParticipantsBySimulationId(simulationId);
            for (var simPart : simPartMapping) {
                var participant = participantDao.findOneById(simPart.getParticipantId());
                var horseSnapshot = horseSnapshotDao.findOneById(participant.getHorseId());
                var jockeySnapshot = jockeySnapshotDao.findOneById(participant.getJockeyId());
                var horseSpeed = ((participant.getLuckFactor() - 0.95) * (horseSnapshot.getMaxSpeed() - horseSnapshot.getMinSpeed())) / (0.1) + horseSnapshot.getMinSpeed();
                horseSpeed = Math.round(horseSpeed*10000)/10000.0;
                double skill = 1 + (0.15 * (1 / Math.PI) * Math.atan(1d / 5 * jockeySnapshot.getSkill()));
                skill = Math.round(skill*10000)/10000.0;
                result.getHorseJockeyCombinations().add(new ParticipantResultDto(simPart.getParticipantId(), 0, horseSnapshot.getName(),
                    jockeySnapshot.getName(), horseSnapshot.getAvgSpeed(), horseSpeed, skill, participant.getLuckFactor()));
            }
            result.setHorseJockeyCombinations(result.getHorseJockeyCombinations().stream()
                .sorted(Comparator.comparing(ParticipantResultDto::getAvgSpeed).reversed())
                .collect(Collectors.toList()));
            for (int i = 1; i <= result.getHorseJockeyCombinations().size(); i++) {
                result.getHorseJockeyCombinations().get(i - 1).setRank(i);
            }
            return result;
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Simulation findOneById(int id) throws ServiceException, NotFoundException {
        try {
            return simulationDao.findOneById(id);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
