package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.service.IJockeyValidationService;
import org.springframework.stereotype.Service;

@Service
public class JockeyValidationService implements IJockeyValidationService {
    @Override
    public void validateIdOfAJockey(Jockey entity) {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("id is required");
        }
    }

    @Override
    public void validateCreationOfAJockey(Jockey entity) throws IllegalArgumentException {
        if (entity.getName() == null || entity.getName().equals("")) {
            throw new IllegalArgumentException("Name must be set");
        } else if (entity.getSkill() == null) {
            throw new IllegalArgumentException("Skill must be set");
        } else if (entity.getSkill().isInfinite() || entity.getSkill().isNaN()) {
            throw new IllegalArgumentException("Skill must not be infinite or NaN");
        }
    }

    @Override
    public void validateIfJockeyIsEmpty(Jockey entity) throws IllegalArgumentException {
        if (entity.getSkill() != null && (entity.getSkill().isInfinite() || entity.getSkill().isNaN())) {
            throw new IllegalArgumentException("Skill must not be infinite or NaN");
        }
        if(entity.getName() != null && entity.getName().equals("")) {
            throw new IllegalArgumentException("Name must be set");
        }
        if (entity.getSkill() == null
            && (entity.getName() == null || entity.getName().equals(""))) {
            throw new IllegalArgumentException("At least one value must be given");
        }
    }
}
