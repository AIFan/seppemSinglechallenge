package at.ac.tuwien.sepm.assignment.individual.rest;

import at.ac.tuwien.sepm.assignment.individual.api.dto.SimulationDto;
import at.ac.tuwien.sepm.assignment.individual.api.dto.SimulationResultDto;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.service.ISimulationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.service.impl.SimulationService;
import at.ac.tuwien.sepm.assignment.individual.util.mapper.SimulationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/simulations")
public class SimulationEndpoint {
    private final ISimulationService simulationService;
    private final SimulationMapper simulationMapper;

    @Autowired
    public SimulationEndpoint(SimulationService simulationService, SimulationMapper simulationMapper) {
        this.simulationService = simulationService;
        this.simulationMapper = simulationMapper;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public SimulationDto getOneById(@PathVariable("id") Integer id) {
        try {
            return simulationMapper.map(simulationService.findOneById(id));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error during read simulation with id " + id, e);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error during reading simulation: " + e.getMessage(), e);
        }
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public SimulationResultDto createSimulation(
        @RequestBody SimulationDto dto
    ) {
        try {
            var created = simulationService.createSimulation(simulationMapper.map(dto));
            return simulationService.getSimulationResultForId(created.getId());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error during creation of simulation: " + e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error during creation of simulation: " + e.getMessage(), e);
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                String.format("Error during creation of a simulation with name %s,", dto.getName()), e);
        }
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public List<SimulationDto> getSimulationsFiltered(
        @RequestParam(value = "name", required = false) String name
    ) {
        SimulationDto dto = new SimulationDto(null, name, null, null);
        try {
            return simulationMapper.map(simulationService.getAllSimulations(simulationMapper.map(dto)));
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error during loading of simulations: " + e.getMessage(), e);
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error during retrieving of all horses: " + e.getMessage(), e);
        }
    }
}
