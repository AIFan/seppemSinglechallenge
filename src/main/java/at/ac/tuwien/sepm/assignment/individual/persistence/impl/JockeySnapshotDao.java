package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.entity.JockeySnapshot;
import at.ac.tuwien.sepm.assignment.individual.persistence.IJockeySnapshotDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JockeySnapshotDao extends AbstractDao<JockeySnapshot> implements IJockeySnapshotDao {
    @Autowired
    public JockeySnapshotDao(DBConnectionManager dbConnectionManager) {
        super(dbConnectionManager);
    }

    @Override
    JockeySnapshot dbResultToEntity(ResultSet result) throws SQLException {
        return new JockeySnapshot(
            result.getInt("id"),
            result.getString("name"),
            result.getDouble("skill"));
    }

    @Override
    public JockeySnapshot createJockeySnapshot(Jockey jockey) throws PersistenceException {
        var snapshot = new JockeySnapshot();
        snapshot.setName(jockey.getName());
        snapshot.setSkill(jockey.getSkill());
        return super.create(snapshot);
    }
}
