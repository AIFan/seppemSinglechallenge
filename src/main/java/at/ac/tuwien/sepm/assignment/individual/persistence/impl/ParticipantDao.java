package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.persistence.IParticipantDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class ParticipantDao extends AbstractDao<Participant> implements IParticipantDao {

    @Autowired
    public ParticipantDao(DBConnectionManager dbConnectionManager) {
        super(dbConnectionManager);
    }

    @Override
    Participant dbResultToEntity(ResultSet result) throws SQLException {
        return new Participant(
            result.getInt("id"),
            result.getDouble("luck_factor"),
            result.getInt("horse_id"),
            result.getInt("jockey_id"));
    }

    @Override
    public Participant createParticipant(Participant entity) throws PersistenceException {
        return super.create(entity);
    }
}
