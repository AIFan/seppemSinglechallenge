package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.entity.HorseSnapshot;
import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.persistence.IHorseSnapshotDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class HorseSnapshotDao extends AbstractDao<HorseSnapshot> implements IHorseSnapshotDao {

    @Autowired
    public HorseSnapshotDao(DBConnectionManager dbConnectionManager) {
        super(dbConnectionManager);
    }

    @Override
    HorseSnapshot dbResultToEntity(ResultSet result) throws SQLException {
        return new HorseSnapshot(
            result.getInt("id"),
            result.getString("name"),
            result.getDouble("min_speed"),
            result.getDouble("max_speed"),
            result.getDouble("avg_speed"));
    }

    @Override
    public HorseSnapshot createHorseSnapshot(Horse entity, Participant participant, Jockey jockey) throws PersistenceException {
        HorseSnapshot snapshot = new HorseSnapshot();
        snapshot.setName(entity.getName());
        snapshot.setMinSpeed(entity.getMinSpeed());
        snapshot.setMaxSpeed(entity.getMaxSpeed());
        snapshot.setAvgSpeed(calculateAvgSpeed(entity.getMinSpeed(), entity.getMaxSpeed(), jockey.getSkill(), participant.getLuckFactor()));
        return super.create(snapshot);
    }

    private double calculateAvgSpeed(double minSpeed, double maxSpeed, double jockeySkill, double luckFactor) {
        double horseSpeed = ((luckFactor - 0.95) * (maxSpeed - minSpeed)) / (0.1) + minSpeed;
        horseSpeed = Math.round(horseSpeed * 10000) / 10000.0;
        double skill = 1 + (0.15 * (1 / Math.PI) * Math.atan(1d / 5 * jockeySkill));
        skill = Math.round(skill * 10000) / 10000.0;
        double res = horseSpeed * skill * luckFactor;
        return Math.round(res * 10000) / 10000.0;
    }
}
