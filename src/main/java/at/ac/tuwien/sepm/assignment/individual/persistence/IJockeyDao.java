package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;

import java.util.List;


/**
 * Represents a data access object for the jockey table.
 */
public interface IJockeyDao {

    /**
     * @param id of the jockey to find.
     * @return the jockey with the specified id.
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the jockey could not be found in the database.
     */
    Jockey findOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity the input parameters to create the new jockey
     * @return the id of the created jockey
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    Jockey createJockey(Jockey entity) throws PersistenceException;

    /**
     * @param entity the input data to update an existing jockey
     * @return the updated jockey
     */
    Jockey updateJockey(Jockey entity) throws PersistenceException, NotFoundException;

    /**
     * @param id of the jockey to delete
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    void deleteOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity a entity containing filter values
     * @return a list containing all jockeys
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    List<Jockey> getAllJockeys(Jockey entity) throws PersistenceException;
}
