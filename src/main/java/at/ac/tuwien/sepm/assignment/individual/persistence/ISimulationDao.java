package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;

import java.util.List;

/**
 * Represents a data access object for the simulation table.
 */
public interface ISimulationDao {
    /**
     * @param id of the simulation to find.
     * @return the simulation with the specified id.
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the simulation could not be found in the database.
     */
    Simulation findOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity the input parameters to create the new simulation
     * @return the id of the created simulation
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    Simulation createSimulation(Simulation entity) throws PersistenceException, NotFoundException;

    /**
     * @param entity a entity containing filter values
     * @return a list containing all simulations
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    List<Simulation> getAllSimulations(Simulation entity) throws PersistenceException;

    /**
     * @param id the id of the simulation
     * @return a list of all participants for this simulation
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the simulation could not be found in the database.
     */
    List<Participant> getParticipantsForSimulationId(Integer id) throws PersistenceException, NotFoundException;
}
