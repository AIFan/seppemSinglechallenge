package at.ac.tuwien.sepm.assignment.individual.api.dto;

public class ParticipantResultDto {
    private int id;
    private int rank;
    private String horseName;
    private String jockeyName;
    private double avgSpeed;
    private double horseSpeed;
    private double skill;
    private double luckFactor;

    public ParticipantResultDto() {
    }

    public ParticipantResultDto(int id, int rank, String horseName, String jockeyName, double avgSpeed, double horseSpeed, double skill, double luckFactor) {
        this.id = id;
        this.rank = rank;
        this.horseName = horseName;
        this.jockeyName = jockeyName;
        this.avgSpeed = avgSpeed;
        this.horseSpeed = horseSpeed;
        this.skill = skill;
        this.luckFactor = luckFactor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    public String getJockeyName() {
        return jockeyName;
    }

    public void setJockeyName(String jockeyName) {
        this.jockeyName = jockeyName;
    }

    public double getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public double getHorseSpeed() {
        return horseSpeed;
    }

    public void setHorseSpeed(double horseSpeed) {
        this.horseSpeed = horseSpeed;
    }

    public double getSkill() {
        return skill;
    }

    public void setSkill(double skill) {
        this.skill = skill;
    }

    public double getLuckFactor() {
        return luckFactor;
    }

    public void setLuckFactor(double luckFactor) {
        this.luckFactor = luckFactor;
    }
}
