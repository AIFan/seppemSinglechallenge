package at.ac.tuwien.sepm.assignment.individual.entity;

import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Column;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Table;

@Table(name = "participant")
public class Participant implements Entity {
    private Integer id;
    @Column(name = "luck_factor")
    private Double luckFactor;
    @Column(name = "horse_id")
    private Integer horseId;
    @Column(name = "jockey_id")
    private Integer jockeyId;

    public Participant() {

    }

    public Participant(Integer id, Double luckFactor, Integer horseId, Integer jockeyId) {
        this.id = id;
        this.luckFactor = luckFactor;
        this.horseId = horseId;
        this.jockeyId = jockeyId;
    }

    public Integer getId() {
        return id;
    }

    public Double getLuckFactor() {
        return luckFactor;
    }

    public void setLuckFactor(Double luckFactor) {
        this.luckFactor = luckFactor;
    }

    public Integer getHorseId() {
        return horseId;
    }

    public void setHorseId(Integer horseId) {
        this.horseId = horseId;
    }

    public Integer getJockeyId() {
        return jockeyId;
    }

    public void setJockeyId(Integer jockeyId) {
        this.jockeyId = jockeyId;
    }
}
