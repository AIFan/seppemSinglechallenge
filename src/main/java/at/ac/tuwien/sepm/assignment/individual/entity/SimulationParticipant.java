package at.ac.tuwien.sepm.assignment.individual.entity;

import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Column;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Table;

@Table(name = "simulationParticipants")
public class SimulationParticipant implements Entity {
    @Column(name = "simulation_id")
    private int simulationId;
    @Column(name = "participant_id")
    private int participantId;

    public SimulationParticipant() {
    }

    public SimulationParticipant(int simulationId, int participantId) {
        this.simulationId = simulationId;
        this.participantId = participantId;
    }

    public int getSimulationId() {
        return simulationId;
    }

    public void setSimulationId(int simulationId) {
        this.simulationId = simulationId;
    }

    public int getParticipantId() {
        return participantId;
    }

    public void setParticipantId(int participantId) {
        this.participantId = participantId;
    }
}
