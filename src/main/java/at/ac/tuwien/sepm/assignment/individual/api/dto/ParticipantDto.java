package at.ac.tuwien.sepm.assignment.individual.api.dto;

public class ParticipantDto {
    private Integer id;
    private Double luckFactor;
    private Integer horseId;
    private Integer jockeyId;

    public ParticipantDto() {
    }

    public ParticipantDto(Integer id, Double luckFactor, Integer horseId, Integer jockeyId) {
        this.id = id;
        this.luckFactor = luckFactor;
        this.horseId = horseId;
        this.jockeyId = jockeyId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLuckFactor() {
        return luckFactor;
    }

    public void setLuckFactor(Double luckFactor) {
        this.luckFactor = luckFactor;
    }

    public Integer getHorseId() {
        return horseId;
    }

    public void setHorseId(Integer horseId) {
        this.horseId = horseId;
    }

    public Integer getJockeyId() {
        return jockeyId;
    }

    public void setJockeyId(Integer jockeyId) {
        this.jockeyId = jockeyId;
    }
}
