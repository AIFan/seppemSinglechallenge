package at.ac.tuwien.sepm.assignment.individual.entity;

import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Column;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Table;

@Table(name = "horseSnapshot")
public class HorseSnapshot implements Entity {
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "min_speed")
    private Double minSpeed;
    @Column(name = "max_speed")
    private Double maxSpeed;

    @Column(name = "avg_speed")
    private Double avgSpeed;

    public HorseSnapshot() {
    }

    public HorseSnapshot(Integer id, String name, Double minSpeed, Double maxSpeed, Double avgSpeed) {
        this.id = id;
        this.name = name;
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
        this.avgSpeed = avgSpeed;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMinSpeed() {
        return minSpeed;
    }

    public void setMinSpeed(Double minSpeed) {
        this.minSpeed = minSpeed;
    }

    public Double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Double getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(Double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }
}
