package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IHorseDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.service.IHorseService;
import at.ac.tuwien.sepm.assignment.individual.service.IHorseValidationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HorseService implements IHorseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HorseService.class);
    private final IHorseDao horseDao;
    private final IHorseValidationService horseValidationService;

    @Autowired
    public HorseService(IHorseDao horseDao, IHorseValidationService horseValidationService) {
        this.horseDao = horseDao;
        this.horseValidationService = horseValidationService;
    }

    @Override
    public Horse findOneById(Integer id) throws ServiceException, NotFoundException {
        LOGGER.info("Get horse with id " + id);
        try {
            return horseDao.findOneById(id);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Horse createHorse(Horse entity) throws ServiceException, IllegalArgumentException {
        LOGGER.info("Create horse with name {}, breed {}, minSpeed {}, maxSpeed {} ", entity.getName(), entity.getBreed(), entity.getMinSpeed(), entity.getMaxSpeed());
        try {
            horseValidationService.validateCreationOfAHorse(entity);
            return horseDao.createHorse(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Horse updateHorse(Horse entity) throws ServiceException, IllegalArgumentException, NotFoundException {
        LOGGER.info("Update horse with id {}", entity.getId());
        try {
            horseValidationService.validateIdOfAHorse(entity);
            horseValidationService.validateIfHorseIsEmpty(entity);
            horseValidationService.validateMinAndMaxSpeedOfAHorse(entity);
            return horseDao.updateHorse(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteOneById(Integer id) throws ServiceException, NotFoundException {
        LOGGER.info("Delete horse with id {}", id);
        try {
            horseDao.deleteOneById(id);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Horse> getAllHorses(Horse entity) throws ServiceException {
        LOGGER.info("Get all horses.");
        try {
            return horseDao.getAllHorses(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
