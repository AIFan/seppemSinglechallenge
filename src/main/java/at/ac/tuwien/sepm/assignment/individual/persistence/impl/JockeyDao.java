package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IJockeyDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


@Repository
public class JockeyDao extends AbstractDao<Jockey> implements IJockeyDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(JockeyDao.class);

    @Autowired
    public JockeyDao(DBConnectionManager dbConnectionManager) {
        super(dbConnectionManager);
    }

    @Override
    Jockey dbResultToEntity(ResultSet result) throws SQLException {
        return new Jockey(
            result.getInt("id"),
            result.getString("name"),
            result.getDouble("skill"),
            result.getTimestamp("created").toLocalDateTime(),
            result.getTimestamp("updated").toLocalDateTime());
    }

    @Override
    public Jockey createJockey(Jockey entity) throws PersistenceException {
        entity.setCreated(LocalDateTime.now());
        entity.setUpdated(LocalDateTime.now());
        return super.create(entity);
    }

    @Override
    public Jockey updateJockey(Jockey entity) throws PersistenceException, NotFoundException {
        String sql = "UPDATE Jockey set name = ?, skill = ?, updated = ? WHERE id = ?";
        try {
            Jockey existing = findOneById(entity.getId());
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setString(1, entity.getName() == null ? existing.getName() : entity.getName());
            statement.setDouble(2, entity.getSkill() == null ? existing.getSkill() : entity.getSkill());
            statement.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
            statement.setInt(4, entity.getId());
            statement.executeUpdate();
            LOGGER.debug("Created jokey with id {}", entity.getId());
            return findOneById(entity.getId());
        } catch (SQLException e) {
            throw new PersistenceException(String.format("Problem while executing SQL update state for jokey with id %d", entity.getId()), e);
        }
    }

    @Override
    public List<Jockey> getAllJockeys(Jockey entity) throws PersistenceException {
        return super.getAll(entity);
    }
}
