package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;


/**
 * Represents a service for validations on the horse
 */
public interface IHorseValidationService {
    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if the id in the entity is null
     */
    void validateIdOfAHorse(Horse entity) throws IllegalArgumentException;

    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if the entity is missing required parameters
     *                                  or validateMinAndMaxSpeedOfAHorse is not valid
     */
    void validateCreationOfAHorse(Horse entity) throws IllegalArgumentException;

    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if the min/max speed are not valid
     */
    void validateMinAndMaxSpeedOfAHorse(Horse entity) throws IllegalArgumentException;

    /**
     * @param entity the entity to validate
     * @throws IllegalArgumentException if no value is set
     */
    void validateIfHorseIsEmpty(Horse entity) throws IllegalArgumentException;
}
