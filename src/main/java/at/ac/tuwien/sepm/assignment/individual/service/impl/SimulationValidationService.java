package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.service.ISimulationValidationService;
import org.springframework.stereotype.Service;

@Service
public class SimulationValidationService implements ISimulationValidationService {

    @Override
    public void validateSimulation(Simulation simulation) throws IllegalArgumentException {
        if (simulation.getName() == null || simulation.getName().equals("")) {
            throw new IllegalArgumentException("name must be set");
        }
        for (var participant : simulation.getParticipants()) {
            if (participant.getLuckFactor() < 0.95 || participant.getLuckFactor() > 1.05) {
                throw new IllegalArgumentException("The luck factor for the participant has to be between 0.95 and 1.05");
            }
        }
    }
}
