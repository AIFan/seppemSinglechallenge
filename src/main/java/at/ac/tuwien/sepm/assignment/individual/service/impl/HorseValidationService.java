package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.service.IHorseValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HorseValidationService implements IHorseValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(HorseValidationService.class);


    @Override
    public void validateIdOfAHorse(Horse entity) {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("id is required");
        }
    }

    @Override
    public void validateCreationOfAHorse(Horse entity) throws IllegalArgumentException {
        if (entity.getName() == null || entity.getName().equals("")) {
            throw new IllegalArgumentException("Name must be set");
        } else if (entity.getMinSpeed() == null) {
            throw new IllegalArgumentException("MinSpeed must be set");
        } else if (entity.getMaxSpeed() == null) {
            throw new IllegalArgumentException("MaxSpeed must be set");

        }
        validateMinAndMaxSpeedOfAHorse(entity);
    }

    @Override
    public void validateMinAndMaxSpeedOfAHorse(Horse entity) throws IllegalArgumentException {
        if ((entity.getMinSpeed() != null && entity.getMinSpeed() < 40)
            || (entity.getMaxSpeed() != null && entity.getMaxSpeed() > 60)
            || (entity.getMinSpeed() != null && entity.getMaxSpeed() != null && entity.getMaxSpeed() < entity.getMinSpeed())) {
            LOGGER.debug("Error during creation of horse min/max speed {}, {} are not valid", entity.getMinSpeed(), entity.getMaxSpeed());
            throw new IllegalArgumentException("Min speed and max speed has to be between 40 and 60 km/h");
        }
    }

    @Override
    public void validateIfHorseIsEmpty(Horse entity) throws IllegalArgumentException  {
        if(entity.getName() != null && entity.getName().equals("")) {
            throw new IllegalArgumentException("Name must be set");
        }
        if((entity.getBreed() == null)
            && entity.getMaxSpeed() == null
            && entity.getMinSpeed() == null
            && (entity.getName() == null)) {
            LOGGER.debug("At least one value must be given");
            throw new IllegalArgumentException("At least one value must be given");
        }
    }

}
