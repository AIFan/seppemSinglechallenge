package at.ac.tuwien.sepm.assignment.individual.util.mapper;

import at.ac.tuwien.sepm.assignment.individual.api.dto.JockeyDto;
import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class JockeyMapper {

    public JockeyDto map(Jockey jockey) {
        return new JockeyDto(jockey.getId(), jockey.getName(), jockey.getSkill(), jockey.getCreated(), jockey.getUpdated());
    }

    public List<JockeyDto> map(List<Jockey> jockeys) {
        return jockeys.stream()
            .map(this::map)
            .collect(Collectors.toList());
    }

    public Jockey map(JockeyDto jockey) {
        return new Jockey(jockey.getId(), jockey.getName(), jockey.getSkill(), jockey.getCreated(), jockey.getUpdated());
    }

}
