package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;

import java.util.List;

/**
 * Represents a service for operations on the horse
 */
public interface IHorseService {

    /**
     * @param id of the horse to find.
     * @return the horse with the specified id.
     * @throws ServiceException  will be thrown if something goes wrong during data processing.
     * @throws NotFoundException will be thrown if the horse could not be found in the system.
     */
    Horse findOneById(Integer id) throws ServiceException, NotFoundException;

    /**
     * @param entity the entity containing the data of the horse to create
     * @return the id of the created horse
     * @throws ServiceException         will be thrown if something goes wrong during data processing.
     * @throws IllegalArgumentException will be thrown if the input parameters don't match the requirements.
     */
    Horse createHorse(Horse entity) throws ServiceException, IllegalArgumentException;

    /**
     * @param entity the entity containing the data of the horse to update
     * @return the updated horse
     * @throws ServiceException         will be thrown if something goes wrong during data processing.
     * @throws IllegalArgumentException will be thrown if the input parameters don't match the requirements.
     */
    Horse updateHorse(Horse entity) throws ServiceException, IllegalArgumentException, NotFoundException;

    /**
     * @param id of the horse to delete
     */
    void deleteOneById(Integer id) throws ServiceException, NotFoundException;

    /**
     * @param entity a entity containing filter values
     * @return a list containing all existing horses
     * @throws ServiceException will be thrown if something goes wrong during data processing.
     */
    List<Horse> getAllHorses(Horse entity) throws ServiceException;
}
