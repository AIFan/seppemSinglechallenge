package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Entity;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Column;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Table;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.QueryHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractDao<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);
    private final Class<?> typeArgument;
    protected final DBConnectionManager dbConnectionManager;

    public AbstractDao(DBConnectionManager dbConnectionManager) {
        Type superclass = getClass().getGenericSuperclass();
        ParameterizedType parameterized = (ParameterizedType) superclass;
        typeArgument = (Class<?>) parameterized.getActualTypeArguments()[0];
        this.dbConnectionManager = dbConnectionManager;
    }

    public T findOneById(Integer id) throws PersistenceException, NotFoundException {
        LOGGER.info(String.format("Get %s with id %d", getTableName(), id));
        String sql = String.format("SELECT * FROM %s WHERE id=?", getTableName());
        T entity = null;
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                entity = dbResultToEntity(result);
            }
        } catch (SQLException e) {
            LOGGER.error("Problem while executing SQL select statement for reading entity with id " + id, e);
            throw new PersistenceException(String.format("Could not read %s with id %d", getTableName(), id), e);
        }
        if (entity != null) {
            return entity;
        } else {
            throw new NotFoundException(String.format("Could not find %s with id %d", getTableName(), id));
        }
    }


    public void deleteOneById(Integer id) throws PersistenceException, NotFoundException {
        LOGGER.debug(String.format("Delete %s with id {}", getTableName()), id);
        String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setInt(1, id);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new NotFoundException(String.format(String.format("Could not find %s with id %%d", getTableName()), id));
            }
        } catch (SQLException e) {
            throw new PersistenceException(String.format(String.format("Problem while executing SQL delete state for %s with id %%d", getTableName()), id), e);
        }
    }


    protected List<T> getAll(Entity entity) throws PersistenceException {
        String sql = String.format("SELECT * FROM %s WHERE %s", getTableName(), QueryHelper.buildFilterQueryString(entity));
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            QueryHelper.setParamsInQuery(statement, entity, false,true );
            ResultSet result = statement.executeQuery();
            List<T> elements = new ArrayList<>();
            while (result.next()) {
                elements.add(dbResultToEntity(result));
            }
            LOGGER.debug("Loaded {} {} entries.", elements.size(), getTableName());
            return elements;
        } catch (SQLException e) {
            throw new PersistenceException("Problem while executing SQL load statement.", e);
        }
    }

    protected Integer getIdFromStatementAndAffectedRows(PreparedStatement statement, Integer affectedRows) throws SQLException {
        try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new SQLException("Operation failed no rows affected");
            }
        }
    }

    public T create(Entity entity) throws PersistenceException {
        String sql = QueryHelper.getInsertQueryFromParams(getFields(), getTableName());
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            QueryHelper.setParamsInQuery(statement, entity, true, false);
            Integer id = getIdFromStatementAndAffectedRows(statement, statement.executeUpdate());
            LOGGER.debug("Created {} with id {}", getTableName(), id);
            return findOneById(id);
        } catch (SQLException e) {
            LOGGER.error("Problem while executing SQL insert statement for creating {}. ", getTableName());
            throw new PersistenceException(String.format("Could not create %s.", getTableName()), e);
        } catch (NotFoundException e) {
            throw new PersistenceException(String.format("Could not create %s.", getTableName()), e);
        }
    }

    abstract T dbResultToEntity(ResultSet result) throws SQLException;

    private String getTableName() {
        return typeArgument.getAnnotation(Table.class).name();
    }

    private List<String> getFields() {

        return Arrays.stream(typeArgument.getDeclaredFields())
            .filter(it -> it.getDeclaredAnnotationsByType(Column.class).length != 0)
            .map(it -> it.getDeclaredAnnotationsByType(Column.class)[0])
            .map(Column::name)
            .collect(Collectors.toList());
    }
}
