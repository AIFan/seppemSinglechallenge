package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.SimulationParticipant;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;

import java.util.List;

/**
 * Represents a data access object for the simulationParticipant table.
 */
public interface ISimulationParticipantDao {
    /**
     * @param simId the id of the simulation
     * @return all participants for the simulation
     */
    List<SimulationParticipant> getAllParticipantsBySimulationId(int simId) throws PersistenceException;
}
