package at.ac.tuwien.sepm.assignment.individual.entity;

import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Column;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Table;

import java.time.LocalDateTime;
import java.util.Objects;

@Table(name = "Jockey")
public class Jockey implements Entity {
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "skill")
    private Double skill;
    @Column(name = "created")
    private LocalDateTime created;
    @Column(name = "updated")
    private LocalDateTime updated;

    public Jockey() {
    }


    public Jockey(Integer id, String name, Double skill, LocalDateTime created, LocalDateTime updated) {
        this.id = id;
        this.name = name;
        this.skill = skill;
        this.created = created;
        this.updated = updated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSkill() {
        return skill;
    }

    public void setSkill(Double skill) {
        this.skill = skill;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Jockey)) return false;
        Jockey jokey = (Jockey) o;
        return Objects.equals(id, jokey.id) &&
            Objects.equals(name, jokey.name) &&
            Objects.equals(skill, jokey.skill) &&
            Objects.equals(created, jokey.created) &&
            Objects.equals(updated, jokey.updated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, skill, created, updated);
    }

    @Override
    public String toString() {
        return "JokeyDto{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", skill=" + skill +
            ", created=" + created +
            ", updated=" + updated +
            '}';
    }
}
