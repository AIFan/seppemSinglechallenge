package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IHorseDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


@Repository
public class HorseDao extends AbstractDao<Horse> implements IHorseDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(HorseDao.class);

    @Autowired
    public HorseDao(DBConnectionManager dbConnectionManager) {
        super(dbConnectionManager);
    }


    @Override
    Horse dbResultToEntity(ResultSet result) throws SQLException {
        return new Horse(
            result.getInt("id"),
            result.getString("name"),
            result.getString("breed"),
            result.getDouble("min_speed"),
            result.getDouble("max_speed"),
            result.getTimestamp("created").toLocalDateTime(),
            result.getTimestamp("updated").toLocalDateTime());
    }

    @Override
    public Horse createHorse(Horse entity) throws PersistenceException {
        entity.setCreated(LocalDateTime.now());
        entity.setUpdated(LocalDateTime.now());
        return super.create(entity);
    }

    @Override
    public Horse updateHorse(Horse entity) throws PersistenceException, NotFoundException {
        String sql = "UPDATE Horse set name = ?, breed = ?, min_speed = ?, max_speed = ?, updated = ? WHERE id = ?";
        try {
            Horse existing = findOneById(entity.getId());
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setString(1, entity.getName() == null ? existing.getName() : entity.getName());
            statement.setString(2, entity.getBreed() == null ? existing.getBreed() : entity.getBreed());
            statement.setDouble(3, entity.getMinSpeed() == null ? existing.getMinSpeed() : entity.getMinSpeed());
            statement.setDouble(4, entity.getMaxSpeed() == null ? existing.getMaxSpeed() : entity.getMaxSpeed());
            statement.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
            statement.setInt(6, entity.getId());
            statement.executeUpdate();
            LOGGER.debug("Created horse with id {}", entity.getId());
            return findOneById(entity.getId());
        } catch (SQLException e) {
            throw new PersistenceException(String.format("Problem while executing SQL update state for horse with id %d", entity.getId()), e);
        }
    }

    @Override
    public List<Horse> getAllHorses(Horse entity) throws PersistenceException {
        return super.getAll(entity);
    }
}
