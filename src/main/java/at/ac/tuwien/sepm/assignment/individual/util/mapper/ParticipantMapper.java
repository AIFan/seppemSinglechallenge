package at.ac.tuwien.sepm.assignment.individual.util.mapper;

import at.ac.tuwien.sepm.assignment.individual.api.dto.ParticipantDto;
import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParticipantMapper {
    public ParticipantDto map(Participant entity) {
        return new ParticipantDto(entity.getId(), entity.getLuckFactor(), entity.getHorseId(), entity.getJockeyId());
    }

    public List<ParticipantDto> map(List<Participant> entities) {
        return entities.stream()
            .map(this::map)
            .collect(Collectors.toList());
    }

    public Participant map(ParticipantDto dto) {
        Participant participant = new Participant();
        participant.setLuckFactor(dto.getLuckFactor());
        participant.setHorseId(dto.getHorseId());
        participant.setJockeyId(dto.getJockeyId());
        return participant;
    }

    public List<Participant> mapDtosToEntity(List<ParticipantDto> entities) {
        return entities.stream()
            .map(this::map)
            .collect(Collectors.toList());
    }
}
