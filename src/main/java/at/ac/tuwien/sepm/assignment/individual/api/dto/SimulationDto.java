package at.ac.tuwien.sepm.assignment.individual.api.dto;

import java.time.LocalDateTime;
import java.util.List;

public class SimulationDto {
    private Integer id;
    private String name;
    private LocalDateTime created;
    private List<ParticipantDto> simulationParticipants;

    public SimulationDto() {
    }

    public SimulationDto(Integer id, String name, LocalDateTime created, List<ParticipantDto> simulationParticipants) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.simulationParticipants = simulationParticipants;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public List<ParticipantDto> getSimulationParticipants() {
        return simulationParticipants;
    }

    public void setSimulationParticipants(List<ParticipantDto> simulationParticipants) {
        this.simulationParticipants = simulationParticipants;
    }
}
