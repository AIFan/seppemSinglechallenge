package at.ac.tuwien.sepm.assignment.individual.util.mapper;

import at.ac.tuwien.sepm.assignment.individual.api.dto.SimulationDto;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.ISimulationDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.impl.SimulationDao;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SimulationMapper {
    private final ParticipantMapper participantMapper;
    private final ISimulationDao simulationDao;

    @Autowired
    public SimulationMapper(ParticipantMapper participantMapper, SimulationDao simulationDao) {
        this.participantMapper = participantMapper;
        this.simulationDao = simulationDao;
    }

    public SimulationDto map(Simulation entity) throws NotFoundException, ServiceException {
        try {
            return new SimulationDto(entity.getId(), entity.getName(), entity.getCreated(),
                participantMapper.map(simulationDao.getParticipantsForSimulationId(entity.getId())));
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public Simulation map(SimulationDto dto) {
        Simulation simulation = new Simulation(dto.getId(), dto.getName(), dto.getCreated());
        if (dto.getSimulationParticipants() == null) {
            simulation.setParticipants(new ArrayList<>());
        } else {
            if (!dto.getSimulationParticipants().isEmpty()) {
                simulation.setParticipants(participantMapper.mapDtosToEntity(dto.getSimulationParticipants()));
            }
        }
        return simulation;
    }

    public List<SimulationDto> map(List<Simulation> allSimulations) throws NotFoundException, ServiceException {
        List<SimulationDto> list = new ArrayList<>();
        for (Simulation allSimulation : allSimulations) {
            SimulationDto map = map(allSimulation);
            list.add(map);
        }
        return list;
    }
}
