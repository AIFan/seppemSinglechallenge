package at.ac.tuwien.sepm.assignment.individual.entity;

/**
 * An empty marker interface for entities
 */
public interface Entity {
}
