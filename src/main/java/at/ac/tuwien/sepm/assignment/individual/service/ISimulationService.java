package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.api.dto.SimulationResultDto;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;

import java.util.List;


/**
 * Represents a service for operations on the simulation
 */
public interface ISimulationService {

    /**
     * @param entity the entity containing the data of the simulation to create
     * @return the id of the created simulation
     * @throws ServiceException will be thrown if something goes wrong during data processing
     * @throws IllegalArgumentException if a validation error occurs
     */
    Simulation createSimulation(Simulation entity) throws ServiceException, NotFoundException, IllegalArgumentException;

    /**
     * @param entity a entity containing filter values
     * @return a list containing all existing jockeys
     * @throws ServiceException will be thrown if something goes wrong during data processing
     */
    List<Simulation> getAllSimulations(Simulation entity) throws ServiceException;

    /**
     * @param simulationId the id of the simulation
     * @return the result of the simulation
     * @throws ServiceException  will be thrown if something goes wrong during data processing
     */
    SimulationResultDto getSimulationResultForId(int simulationId) throws ServiceException, NotFoundException;

    /**
     * @param id of the simulation to find.
     * @return the jockey with the specified id.
     * @throws ServiceException  will be thrown if something goes wrong during data processing.
     * @throws NotFoundException will be thrown if the jockey could not be found in the system.
     */
    Simulation findOneById(int id) throws ServiceException, NotFoundException;
}
