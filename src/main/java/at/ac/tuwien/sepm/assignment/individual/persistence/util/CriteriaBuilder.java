package at.ac.tuwien.sepm.assignment.individual.persistence.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class CriteriaBuilder {
    private CriteriaBuilder() {
        //private to avoid initialization
    }

    public static String appendEqualsParam(String name, Object param, String query) {
        if (param != null) {
            if (!query.equals("")) {
                return String.format("%s AND %s = ?", query, name);
            }
            return String.format("%s = ?", name);
        }
        return query;
    }

    public static String appendGreaterOrEqualsParam(String name, Object param, String query) {
        if (param != null) {
            if (!query.equals("")) {
                return String.format("%s AND %s >= ?", query, name);
            }
            return String.format("%s >= ?", name);
        }
        return query;
    }

    public static String appendLesserOrEqualsParam(String name, Object param, String query) {
        if (param != null) {
            if (!query.equals("")) {
                return String.format("%s AND %s <= ?", query, name);
            }
            return String.format("%s <= ?", name);
        }
        return query;
    }

    public static String appendLikeParam(String name, Object param, String query) {
        if (param != null) {
            if (!query.equals("")) {
                return String.format("%s AND %s like ?", query, name);
            }
            return String.format("%s like ?", name);
        }
        return query;
    }

    static int addParamToStatement(Object param, PreparedStatement statement, int currentIndex, boolean nullable) throws SQLException {
        if (param != null || nullable) {
            if (param instanceof String) {
                statement.setString(currentIndex, (String) param);
            }
            if (param instanceof Double) {
                statement.setDouble(currentIndex, (Double) param);
            }
            if (param instanceof LocalDateTime) {
                statement.setTimestamp(currentIndex, Timestamp.valueOf((LocalDateTime) param));
            }
            if (param instanceof Integer) {
                statement.setInt(currentIndex, (Integer) param);
            }
            if (param == null) {
                statement.setObject(currentIndex, null);
            }
            return currentIndex + 1;
        }
        return currentIndex;
    }
}
