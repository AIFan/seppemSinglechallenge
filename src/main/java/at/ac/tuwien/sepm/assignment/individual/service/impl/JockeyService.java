package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Jockey;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.IJockeyDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.service.IJockeyService;
import at.ac.tuwien.sepm.assignment.individual.service.IJockeyValidationService;
import at.ac.tuwien.sepm.assignment.individual.service.exceptions.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JockeyService implements IJockeyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JockeyService.class);
    private final IJockeyDao jockeyDao;
    private final IJockeyValidationService jockeyValidationService;

    @Autowired
    public JockeyService(IJockeyDao jockeyDao, IJockeyValidationService jockeyValidationService) {
        this.jockeyDao = jockeyDao;
        this.jockeyValidationService = jockeyValidationService;
    }

    @Override
    public Jockey findOneById(Integer id) throws ServiceException, NotFoundException {
        LOGGER.info("Get jockey with id " + id);
        try {
            return jockeyDao.findOneById(id);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Jockey createJockey(Jockey entity) throws ServiceException, IllegalArgumentException {
        LOGGER.info("Create jockey with name {} and skill {}", entity.getName(), entity.getSkill());
        try {
            jockeyValidationService.validateCreationOfAJockey(entity);
            return jockeyDao.createJockey(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Jockey updateJockey(Jockey entity) throws ServiceException, IllegalArgumentException, NotFoundException {
        LOGGER.info("Update jockey with id {}", entity.getId());
        try {
            jockeyValidationService.validateIdOfAJockey(entity);
            jockeyValidationService.validateIfJockeyIsEmpty(entity);
            return jockeyDao.updateJockey(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteOneById(Integer id) throws ServiceException, NotFoundException {
        LOGGER.info("Delete jockey with id {}", id);
        try {
            jockeyDao.deleteOneById(id);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Jockey> getAllJockeys(Jockey entity) throws ServiceException {
        LOGGER.info("Get all jockeys.");
        try {
            return jockeyDao.getAllJockeys(entity);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
