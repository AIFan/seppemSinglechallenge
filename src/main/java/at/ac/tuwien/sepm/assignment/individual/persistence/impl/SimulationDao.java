package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.*;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SimulationDao extends AbstractDao<Simulation> implements ISimulationDao {
    private final IParticipantDao participantDao;
    private final IHorseSnapshotDao horseSnapshotDao;
    private final IHorseDao horseDao;
    private final IJockeySnapshotDao jockeySnapshotDao;
    private final IJockeyDao jockeyDao;
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationDao.class);

    @Autowired
    public SimulationDao(DBConnectionManager dbConnectionManager, IParticipantDao participantDao, IHorseSnapshotDao horseSnapshotDao, HorseDao horseDao, IJockeySnapshotDao jockeySnapshotDao, JockeyDao jockeyDao) {
        super(dbConnectionManager);
        this.participantDao = participantDao;
        this.horseSnapshotDao = horseSnapshotDao;
        this.horseDao = horseDao;
        this.jockeySnapshotDao = jockeySnapshotDao;
        this.jockeyDao = jockeyDao;
    }

    @Override
    Simulation dbResultToEntity(ResultSet result) throws SQLException {
        return new Simulation(result.getInt("id"),
            result.getString("name"),
            result.getTimestamp("created").toLocalDateTime());
    }

    @Override
    public Simulation createSimulation(Simulation entity) throws PersistenceException, NotFoundException {
        entity.setCreated(LocalDateTime.now());
        var sim = super.create(entity);
        var participants = new ArrayList<Participant>();
        if (entity.getParticipants() != null && !entity.getParticipants().isEmpty()) {
            for (Participant participant : entity.getParticipants()) {
                var horse = horseDao.findOneById(participant.getHorseId());
                var jockey = jockeyDao.findOneById(participant.getJockeyId());
                var horseSnapshot = horseSnapshotDao.createHorseSnapshot(horse, participant, jockey);
                var jockeySnapshot = jockeySnapshotDao.createJockeySnapshot(jockey);
                participant.setHorseId(horseSnapshot.getId());
                participant.setJockeyId(jockeySnapshot.getId());
                participant = participantDao.createParticipant(participant);
                createSimulationToParticipantMapping(sim.getId(), participant.getId());
                participants.add(participantDao.createParticipant(participant));
            }
        }

        sim.setParticipants(participants);
        return sim;
    }

    private void createSimulationToParticipantMapping(Integer simId, Integer participantId) throws PersistenceException {
        var sql = "INSERT INTO simulationParticipants VALUES (?,?)";
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setInt(1, simId);
            statement.setInt(2, participantId);
            statement.executeUpdate();
            LOGGER.debug("Created participant with sim id {} and participant id {}", simId, participantId);
        } catch (SQLException e) {
            LOGGER.error("Problem while executing SQL insert statement for creating simulation to participant mapping. ");
            throw new PersistenceException(String.format("Could not create simulation mapping with id %d.", simId), e);
        }
    }

    @Override
    public List<Participant> getParticipantsForSimulationId(Integer simId) throws PersistenceException, NotFoundException {
        var participants = new ArrayList<Participant>();
        var sql = "SELECT * FROM simulationParticipants WHERE simulation_id = ?";
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setInt(1, simId);
            var result = statement.executeQuery();
            while (result.next()) {
                var partId = result.getInt("participant_id");
                participants.add(participantDao.findOneById(partId));
            }
            return participants;
        } catch (SQLException e) {
            LOGGER.error("Problem while executing SQL query statement for loading simulation to participant mapping. ");
            throw new PersistenceException(String.format("Could not load simulation mapping with simulation id %d.", simId), e);
        }
    }

    @Override
    public List<Simulation> getAllSimulations(Simulation entity) throws PersistenceException {
        return super.getAll(entity);
    }
}
