package at.ac.tuwien.sepm.assignment.individual.persistence.impl;

import at.ac.tuwien.sepm.assignment.individual.entity.SimulationParticipant;
import at.ac.tuwien.sepm.assignment.individual.persistence.ISimulationParticipantDao;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;
import at.ac.tuwien.sepm.assignment.individual.persistence.util.DBConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SimulationParticipantDao implements ISimulationParticipantDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationParticipantDao.class);
    private final DBConnectionManager dbConnectionManager;

    @Autowired
    public SimulationParticipantDao(DBConnectionManager dbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public List<SimulationParticipant> getAllParticipantsBySimulationId(int simId) throws PersistenceException {
        var participants = new ArrayList<SimulationParticipant>();
        var sql = "SELECT * FROM simulationParticipants WHERE simulation_id = ?";
        try {
            PreparedStatement statement = dbConnectionManager.getConnection().prepareStatement(sql);
            statement.setInt(1, simId);
            var result = statement.executeQuery();
            while (result.next()) {
                participants.add(new SimulationParticipant(result.getInt("simulation_id"),
                    result.getInt("participant_id")));
            }
            return participants;
        } catch (SQLException e) {
            LOGGER.error("Problem while executing SQL query statement for loading simulation to participant mapping. ");
            throw new PersistenceException(String.format("Could not load simulation mapping with simulation id %d.", simId), e);
        }
    }
}
