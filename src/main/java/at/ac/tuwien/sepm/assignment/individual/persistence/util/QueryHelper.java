package at.ac.tuwien.sepm.assignment.individual.persistence.util;

import at.ac.tuwien.sepm.assignment.individual.entity.*;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class QueryHelper {
    private QueryHelper() {
        //private constructor to avoid initialisation
    }

    public static String buildFilterQueryString(Entity entity) {
        if (entity instanceof Horse) {
            return buildHorseFilterQueryString((Horse) entity);
        }
        if (entity instanceof Jockey) {
            return buildJockeyFilterQueryString((Jockey) entity);
        }
        if (entity instanceof Simulation) {
            return buildSimulationFilterQueryString((Simulation) entity);
        }
        throw new UnsupportedOperationException(String.format("The class %s is not supported!", entity.getClass().getName()));
    }

    private static String buildHorseFilterQueryString(Horse entity) {
        String filterValues = "";
        filterValues = CriteriaBuilder.appendLikeParam("name", entity.getName(), filterValues);
        filterValues = CriteriaBuilder.appendLikeParam("breed", entity.getBreed(), filterValues);
        filterValues = CriteriaBuilder.appendGreaterOrEqualsParam("min_speed", entity.getMinSpeed(), filterValues);
        filterValues = CriteriaBuilder.appendLesserOrEqualsParam("max_speed", entity.getMaxSpeed(), filterValues);
        if (entity.getName() == null && entity.getBreed() == null && entity.getMinSpeed() == null && entity.getMaxSpeed() == null) {
            filterValues = "true";
        }
        return filterValues;
    }

    private static String buildJockeyFilterQueryString(Jockey entity) {
        String filterValues = "";
        filterValues = CriteriaBuilder.appendLikeParam("name", entity.getName(), filterValues);
        filterValues = CriteriaBuilder.appendGreaterOrEqualsParam("skill", entity.getSkill(), filterValues);
        if (entity.getName() == null && entity.getSkill() == null) {
            filterValues = "1";
        }
        return filterValues;
    }

    private static String buildSimulationFilterQueryString(Simulation entity) {
        String filterValues = CriteriaBuilder.appendLikeParam("name", entity.getName(), "");
        if (entity.getName() == null) {
            filterValues = "true";
        }
        return filterValues;
    }

    public static void setParamsInQuery(PreparedStatement statement, Entity entity, boolean allowNullVales, boolean isFilterQuery) throws SQLException {
        if (entity instanceof Horse) {
            setHorseParamsInQuery(statement, (Horse) entity, allowNullVales, isFilterQuery);
        } else if (entity instanceof Jockey) {
            setJockeyParamsInQuery(statement, (Jockey) entity, isFilterQuery);
        } else if (entity instanceof Simulation) {
            setSimulationParamsInQuery(statement, (Simulation) entity, isFilterQuery);
        } else if (entity instanceof Participant) {
            setParticipantParamsInQuery(statement, (Participant) entity);
        } else if (entity instanceof HorseSnapshot) {
            setHorseSnapshotParamsInQuery(statement, (HorseSnapshot) entity);
        } else if (entity instanceof JockeySnapshot) {
            setJockeySnapshotParamsInQuery(statement, (JockeySnapshot) entity);
        } else {
            throw new UnsupportedOperationException(String.format("The class %s is not supported!", entity.getClass().getName()));
        }
    }

    private static void setJockeyParamsInQuery(PreparedStatement statement, Jockey entity, boolean isFilterQuery) throws SQLException {
        int currentIndex = 1;
        currentIndex = CriteriaBuilder.addParamToStatement(!isFilterQuery || entity.getName() == null ? entity.getName() : String.format("%%%s%%", entity.getName())
            , statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getSkill(), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getCreated(), statement, currentIndex, false);
        CriteriaBuilder.addParamToStatement(entity.getUpdated(), statement, currentIndex, false);
    }

    private static void setSimulationParamsInQuery(PreparedStatement statement, Simulation entity, boolean isFilterQuery) throws SQLException {
        int currentIndex = 1;
        currentIndex = CriteriaBuilder.addParamToStatement(!isFilterQuery || entity.getName() == null ? entity.getName() : String.format("%%%s%%", entity.getName()), statement, currentIndex, false);
        CriteriaBuilder.addParamToStatement(entity.getCreated(), statement, currentIndex, false);
    }

    private static void setParticipantParamsInQuery(PreparedStatement statement, Participant entity) throws SQLException {
        int currentIndex = 1;
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getLuckFactor(), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getHorseId(), statement, currentIndex, false);
        CriteriaBuilder.addParamToStatement(entity.getJockeyId(), statement, currentIndex, false);
    }

    private static void setHorseParamsInQuery(PreparedStatement statement, Horse entity, boolean allowNullVales, boolean isFilerQuery) throws SQLException {
        int currentIndex = 1;
        currentIndex = CriteriaBuilder.addParamToStatement(!isFilerQuery || entity.getName() == null ? entity.getName() :
            String.format("%%%s%%", entity.getName()), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(!isFilerQuery || entity.getBreed() == null ? entity.getBreed() :
            String.format("%%%s%%", entity.getBreed()), statement, currentIndex, allowNullVales);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getMinSpeed(), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getMaxSpeed(), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getCreated(), statement, currentIndex, false);
        CriteriaBuilder.addParamToStatement(entity.getUpdated(), statement, currentIndex, false);
    }

    private static void setHorseSnapshotParamsInQuery(PreparedStatement statement, HorseSnapshot entity) throws SQLException {
        int currentIndex = 1;
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getName(), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getMinSpeed(), statement, currentIndex, false);
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getMaxSpeed(), statement, currentIndex, false);
        CriteriaBuilder.addParamToStatement(entity.getAvgSpeed(), statement, currentIndex, false);
    }

    private static void setJockeySnapshotParamsInQuery(PreparedStatement statement, JockeySnapshot entity) throws SQLException {
        int currentIndex = 1;
        currentIndex = CriteriaBuilder.addParamToStatement(entity.getName(), statement, currentIndex, false);
        CriteriaBuilder.addParamToStatement(entity.getSkill(), statement, currentIndex, false);
    }

    public static String getInsertQueryFromParams(List<String> fields, String table) {
        StringBuilder formattedFields = new StringBuilder();
        StringBuilder params = new StringBuilder();
        for (int i = 0; i < fields.size() - 1; i++) {
            formattedFields.append(fields.get(i)).append(",");
            params.append("?,");
        }
        params.append("?");
        formattedFields.append(fields.get(fields.size() - 1));
        return String.format("INSERT INTO %s(%s) VALUES (%s)", table, formattedFields.toString(), params.toString());
    }
}
