package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entity.Simulation;


/**
 * Represents a service for the validation of the simulation.
 */
public interface ISimulationValidationService {
    void validateSimulation(Simulation sim);
}
