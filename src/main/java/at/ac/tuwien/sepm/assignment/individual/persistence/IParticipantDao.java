package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Participant;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;

/**
 * Represents a data access object for the participant table.
 */
public interface IParticipantDao {
    /**
     * @param id of the participant to find.
     * @return the participant with the specified id.
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the participant could not be found in the database.
     */
    Participant findOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity the input parameters to create the new participant
     * @return the id of the created participant
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    Participant createParticipant(Participant entity) throws PersistenceException;

    /**
     * @param id of the participant to delete
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    void deleteOneById(Integer id) throws PersistenceException, NotFoundException;


}
