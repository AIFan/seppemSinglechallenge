package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entity.Horse;
import at.ac.tuwien.sepm.assignment.individual.exceptions.NotFoundException;
import at.ac.tuwien.sepm.assignment.individual.persistence.exceptions.PersistenceException;

import java.util.List;

/**
 * Represents a data access object for the horse table.
 */
public interface IHorseDao {

    /**
     * @param id of the horse to find.
     * @return the horse with the specified id.
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     * @throws NotFoundException    will be thrown if the horse could not be found in the database.
     */
    Horse findOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity the input parameters to create the new horse
     * @return the id of the created horse
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    Horse createHorse(Horse entity) throws PersistenceException;

    /**
     * @param entity the input data to update an existing horse
     * @return the updated horse
     */
    Horse updateHorse(Horse entity) throws PersistenceException, NotFoundException;

    /**
     * @param id of the horse to delete
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    void deleteOneById(Integer id) throws PersistenceException, NotFoundException;

    /**
     * @param entity a entity containing filter values
     * @return a list containing all horses
     * @throws PersistenceException will be thrown if something goes wrong during the database access.
     */
    List<Horse> getAllHorses(Horse entity) throws PersistenceException;
}
