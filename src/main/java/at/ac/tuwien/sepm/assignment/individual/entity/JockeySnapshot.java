package at.ac.tuwien.sepm.assignment.individual.entity;

import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Column;
import at.ac.tuwien.sepm.assignment.individual.persistence.annotations.Table;

@Table(name = "jockeySnapshot")
public class JockeySnapshot implements Entity{
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "skill")
    private Double skill;

    public JockeySnapshot() {
    }

    public JockeySnapshot(Integer id, String name, Double skill) {
        this.id = id;
        this.name = name;
        this.skill = skill;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSkill() {
        return skill;
    }

    public void setSkill(Double skill) {
        this.skill = skill;
    }
}
