-- create table horse if not exists
CREATE TABLE IF NOT EXISTS horse
(
  -- use auto incrementing IDs as primary key
  id        BIGINT AUTO_INCREMENT PRIMARY KEY,
  name      VARCHAR(255) NOT NULL,
  breed     TEXT,
  min_speed DOUBLE       NOT NULL,
  max_speed DOUBLE       NOT NULL,
  created   DATETIME     NOT NULL,
  updated   DATETIME     NOT NULL
);

-- create snapshot table for the simulations
CREATE TABLE IF NOT EXISTS horseSnapshot
(
  -- use auto incrementing IDs as primary key
  id        BIGINT AUTO_INCREMENT PRIMARY KEY,
  name      VARCHAR(255) NOT NULL,
  min_speed DOUBLE       NOT NULL,
  max_speed DOUBLE       NOT NULL,
  avg_speed DOUBLE
);

CREATE TABLE IF NOT EXISTS jockey
(
  -- use auto incrementing IDs as primary key
  id      BIGINT AUTO_INCREMENT PRIMARY KEY,
  name    VARCHAR(255) NOT NULL,
  skill   DOUBLE       NOT NULL,
  created DATETIME     NOT NULL,
  updated DATETIME     NOT NULL
);

-- create snapshot table for the simulations
CREATE TABLE IF NOT EXISTS jockeySnapshot
(
  -- use auto incrementing IDs as primary key
  id    BIGINT AUTO_INCREMENT PRIMARY KEY,
  name  VARCHAR(255) NOT NULL,
  skill DOUBLE       NOT NULL
);

CREATE TABLE IF NOT EXISTS simulation
(
  -- use auto incrementing IDs as primary key
  id      BIGINT AUTO_INCREMENT PRIMARY KEY,
  name    VARCHAR(255) NOT NULL,
  created DATETIME     NOT NULL
);

CREATE TABLE IF NOT EXISTS participant
(
  -- use auto incrementing IDs as primary key
  id          BIGINT AUTO_INCREMENT PRIMARY KEY,
  luck_factor DOUBLE NOT NULL,
  horse_id    BIGINT NOT NULL REFERENCES horseSnapshot (id),
  jockey_id   BIGINT NOT NULL REFERENCES jockeySnapshot (id)
);

CREATE TABLE IF NOT EXISTS simulationParticipants
(
  simulation_id  BIGINT NOT NULL REFERENCES simulation (id),
  participant_id BIGINT NOT NULL REFERENCES participant (id)
);

ALTER TABLE simulationParticipants
  ADD PRIMARY KEY (simulation_id, participant_id)