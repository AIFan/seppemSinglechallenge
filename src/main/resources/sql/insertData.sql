-- insert initial test data
-- the id is hardcode to enable references between further test data
DELETE FROM HORSE;
DELETE FROM JOCKEY;
INSERT INTO horse (ID, NAME, BREED, MIN_SPEED, MAX_SPEED, CREATED, UPDATED)
VALUES (1, 'Joe', 'Cob', 40.1, 50.0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
       (2, 'Lisa', 'Arab', 40.5, 50.7, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
       (3, 'Jim', 'Andalusian', 40.0, 60.0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO jockey (ID, NAME, SKILL, CREATED, UPDATED)
VALUES (1, 'Hans', 1.11, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
       (2, 'Peter', 11.11, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());